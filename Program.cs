﻿using System;

namespace loops_example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var counter = 20;
            var a = (Int32) 0;
            var b = (Int32) 0;
            
            for (a = 0; a < counter; a++)
            {
                while (b == a + 1)
                {
                    if (b % 2 == 0)
                    {
                        Console.WriteLine($"This is line number {b}");
                    }
                }
            }
            }
        }
    }
}
